def is_prime(num):
    """Checks if a number is a prime, returns bool"""
    if num % 2 == 0:
        return False
    for n in range(3, int(num/2) + 1, 2):
        if (num % n) == 0:
            return False
    return True


def odd_factors(n):
    """Returns all odd factors of n"""
    results = set()
    for i in range(1, int(n**0.5) + 1):
        div, mod = divmod(n, i)
        if mod == 0:
            if div % 2 != 0 and i % 2 != 0:
                results |= {i, int(div)}
            else:
                if div % 2 != 0:
                    results |= {int(div)}
    return results


def largest_prime_factor(n):
    """Finds the largest prime factor of n"""
    all_factors = odd_factors(n)
    for num in sorted(all_factors)[::-1]:
        if is_prime(num):
            return num

num = largest_prime_factor(600851475143)
print(num)
