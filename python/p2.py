even_sum = 0

last_term = 0 
second_to_last_term = 1 

n = 0
while n < 4e6:
    n = second_to_last_term + last_term
    if n % 2 == 0:
        even_sum += n
    last_term = second_to_last_term
    second_to_last_term = n

print(even_sum)
