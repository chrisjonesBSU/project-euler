palindromes = set() 
for i in reversed(range(100, 1000)):
    for j in reversed(range(100, 1000)):
        prod = str(i*j) 
        prod_rev = prod[::-1] 
        if prod == prod_rev:
            palindromes.add(i*j)

print(sorted(list(palindromes))[-1])
