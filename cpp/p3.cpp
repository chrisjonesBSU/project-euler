// Your First C++ Program
#include <iostream>
#include <vector>
#include <tuple>
#include <cmath>
#include <algorithm>


bool isPrime(int num) {
	// Checks if num is prime; returns bool
	if (num % 2 == 0){
		return false;
	}
	bool prime = true;
	for (int i=3; i<(num/2)+1; i+=2){
		if (num % i == 0){
			prime = false;
			break;
		}
	}
	return prime;
}


std::vector<int> oddFactors(unsigned long int n){
	// Returns only odd factors of n
	std::vector<int> factors(0); // Q: How to make/use sets in C++?
	unsigned long int root_n = sqrt(n);
	unsigned long int div;
	unsigned long int mod;
	for (int i=1; i<root_n + 1; i++){
		div = n / i;
		mod = n % i;
		if (mod == 0){
			if (div %2 != 0 && i %2 != 0){
				factors.push_back (div);
				factors.push_back (i);
			}else if (div %2 != 0 && i %2 == 0){
				factors.push_back (div);
			}
		}
	}
	std::sort (factors.begin(), factors.end());
	std::reverse (factors.begin(), factors.end());
	return factors;
}


int main() {
	static unsigned long int check_num = 600851475143;
	std::vector<int> factors;
	factors = oddFactors(check_num);
	for (int i: factors){
		bool prime_factor = isPrime(i);
		if (prime_factor){
			std::cout << i << "\n";
			break;
		}
	}
    return 0;
}
