// Your First C++ Program
#include <iostream>
#include <vector>


int sumofEvenFibs(int max) {
	int even_sum = 0;
	int n0 = 0;
	int n1 = 1;
	do{
		int n = n1 + n0;
		if (n%2 == 0){
			even_sum += n;
		}
		n0 = n1;
		n1 = n;
	}
	while (n1 < max);
	return even_sum;
}


int main() {
	int sum;
	sum = sumofEvenFibs(4e6);
	std::cout << sum << "\n";
    return 0;
}
