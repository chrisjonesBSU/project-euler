// Your First C++ Program
#include <iostream>
#include <vector>
# include <algorithm>

int reverseInt(int num){
	// Returns reversed integer of a given positive integer
	int rev_num = 0;
	while (num > 0){
		int last_digit = num % 10;
		rev_num = rev_num*10 + last_digit;
		num /= 10;
	}
	return rev_num;
}

int main() {
	std::vector<int> palindromes(0);
	int prod;
	int rev_prod;
	for (int i=999; i>0; i--){
		for (int j=999; j>0; j--){
			prod = i*j;
			rev_prod = reverseInt(prod);
			if (prod == rev_prod){
				palindromes.push_back (prod);
			}
		}
	}
	std::sort (palindromes.begin(), palindromes.end());
	std::reverse (palindromes.begin(), palindromes.end());
	std::cout << palindromes[0] << "\n";
}
