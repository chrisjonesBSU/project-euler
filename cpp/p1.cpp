// Your First C++ Program
#include <iostream>
#include <vector>


int sumofMultiples(int start, int stop) {
	int sum = 0;
	for (int i=start; i<stop; i++){
		if (i%3 == 0 || i%5 == 0){
			sum += i;
		}
	}
	return sum;
}


int main() {
	int sum;
	sum = sumofMultiples(0, 1000);
	std::cout << sum << "\n";
    return 0;
}
