package main

import "fmt"


func sum_of_multiples(start int, stop int) int {
	sum := 0
	for i := start; i < stop; i++ {
		if (i %3 ==0 || i %5 == 0) {
			sum += i
		}
	}
	return sum
}

func main() {
	sum := sum_of_multiples(0, 1000)
	fmt.Println(sum)
}
