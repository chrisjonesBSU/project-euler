package main

import ( 
	"fmt"
	"math"
	"sort"
)


func is_prime(num int) bool {
	// Checks if num is prime; returns bool
	if (num % 2 == 0){
		return false
	}
	for i := 3; i < (num/2) + 1; i+=2{
		if (num % i == 0){
			return false
		}
	}
	return true
}


func divmod(m int, n int) []int{
	// Python has this built in; making same thing for Go
	// Not needed; just doing for fun
	// Given numbers m and n; returns (m/n; m%n)
	var results []int
	results = append(results, int(m/n))
	results = append(results, m%n)
	return results // Q: How to return multiple values in Go?
}


func odd_factors(n int) []int{
	// Returns slice of odd-only factors of n
	var results []int
	var end = int(math.Sqrt(float64(n))) + 1
	for i := 1; i<end; i++ {
		var div_mod = divmod(n, i) // Q: Create 2 vars in single func call?
		var div = div_mod[0]
		var mod = div_mod[1]
		if mod == 0{
			if (div %2 != 0 && i %2 !=0){
				results = append(results, div, i)
			} else if (div %2 !=0 && i %2 == 0){
				results = append(results, div)
			}
		}
	}
	return results
}


func main() {
	var factors = odd_factors(600851475143)
	sort.Sort(sort.Reverse(sort.IntSlice(factors)))
	for i := 0; i<len(factors); i++{
		if is_prime(factors[i]){
				fmt.Println(factors[i])
				break
			}
		}
	}
