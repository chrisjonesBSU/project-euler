package main

import (
	"fmt"
	"sort"
	"strconv"
)

func runeSlicesEqual(a []rune, b []rune) bool{
	if len(a) != len(b){
		return false
	}
	for i, v := range a{
		if v != b[i]{
			return false
		}
	}
	return true
}
		
// TODO; Am I repeating pairs of i and j?
func main() {
	var palindromes []int
	for i := 999; i > 99; i-- {
		for j := 999; j > 99; j-- {
			var prod = i*j
			var prod_str = strconv.Itoa(prod) 
			runeSlice := []rune(prod_str)
			var revSlice []rune
			for i:= len(runeSlice)-1; i>=0; i--{
				revSlice = append(revSlice, runeSlice[i])
			}
			if runeSlicesEqual(runeSlice, revSlice){
				palindromes = append(palindromes, prod)
			}
		}
	}
	sort.Sort(sort.Reverse(sort.IntSlice(palindromes)))
	fmt.Println(palindromes[0])
}
