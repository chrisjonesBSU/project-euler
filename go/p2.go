package main

import "fmt"


func sum_even_fibs(max int) int {
	even_sum := 0
	n0 := 0
	n1 := 1

	for i := 2; i < max; i= n1+n0 {
		if i %2 == 0{
			even_sum += i
		}
		n0 = n1
		n1 = i
	}
	return even_sum

}

func main() {
	sum := sum_even_fibs(4e6)
	fmt.Println(sum)
}
